const mongoose = require('mongoose')
const Product = require('../models/Product')
mongoose.connect('mongodb://localhost:27017/example')

async function main () {
  await clearProduct()

  //! สร้างข้อมูล และบันทึกลง collection Product
  for (let i = 1; i <= 12; i++) {
    const product = new Product({ name: `Product ${i}`, price: 12000 })
    product.save()
  }
}
//! function clear DB
async function clearProduct () {
  await Product.deleteMany({})
}

main().then(() => console.log('Finish'))
