const express = require('express')
const router = express.Router()
const Product = require('../models/Product')
// eslint-disable-next-line no-unused-vars
const products = [
  { id: 1, name: 'iPad gen9 64G Wifi', price: 10000 },
  { id: 2, name: 'iPad gen10 64G Wifi', price: 10005 },
  { id: 3, name: 'iPad gen11 64G Wifi', price: 10005 },
  { id: 4, name: 'iPad gen12 64G Wifi', price: 10006 },
  { id: 5, name: 'iPad gen5 64G Wifi', price: 10007 },
  { id: 6, name: 'iPad gen7 64G Wifi', price: 10008 },
  { id: 7, name: 'iPad gen8 64G Wifi', price: 10009 },
  { id: 8, name: 'iPad gen9 64G Wifi', price: 100010 },
  { id: 9, name: 'iPad gen10 64G Wifi', price: 1000111 },
  { id: 10, name: 'iPad gen11 64G Wifi', price: 1000555 }
]

const getProducts = async function (req, res, next) {
  try {
    const products = await Product.find({}).exec()
    res.status(200).json(products)
  } catch (err) {
    return res.status(500).send({
      massage: err.massage
    })
  }
}

const getProduct = async function (req, res, next) {
  const id = req.params.id
  try {
    const product = await Product.findById(id).exec()
    res.status(200).json(product)
  } catch (err) {
    return res.status(404).json({
      message: err.message
    })
  }
}

const addProducuts = async function (req, res, next) {
  const newProduct = new Product({
    name: req.body.name,
    price: parseFloat(req.body.price)
  })
  try {
    await newProduct.save()
    res.status(201).json(newProduct)
  } catch (err) {
    return res.status(500).send({
      massage: err.massage
    })
  }
}

const updateProducut = async function (req, res, next) {
  const productId = req.params.id
  try {
    const product = await Product.findById(productId)
    product.name = req.body.name
    product.price = parseFloat(req.body.price)
    await product.save()
    res.status(200).send(product)
  } catch (err) {
    return res.status(500).send({
      massage: err.massage
    })
  }
}

const deleteProducut = async function (req, res, next) {
  const index = req.params.id
  try {
    const deletedProduct = await Product.findByIdAndDelete(index).exec()
    res.status(200).send(
      deletedProduct
    )
  } catch (err) {
    return res.status(500).send({
      massage: err.massage
    })
  }
}

router.get('/', getProducts)
router.get('/:id', getProduct)
router.post('/', addProducuts)
router.put('/:id', updateProducut)
router.delete('/:id', deleteProducut)

module.exports = router
